#!/usr/bin/env python
"""
Copyright (C) by ETHZ/SED

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

Author of the Software: Camilo Munoz
"""
import dash
from dash import Dash, dcc, html, Input, Output, callback, dash_table, no_update, State
import json, glob, os, datetime, time
import dash_bootstrap_components as dbc
import pandas as pd
from pandas import json_normalize
from operator import itemgetter
import plotly.express as px
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.figure_factory as ff
import math
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
import plotly.io as pio
import numpy as np
from seiscomp import system, logging
from obspy import read_events
from obspy.clients.fdsn import Client
from obspy import UTCDateTime
import dash_leaflet as dl
import dash_leaflet.express as dlx
import base64
from dash_extensions.javascript import assign
import dash_mantine_components as dmc
import configparser
import subprocess
from dash.exceptions import PreventUpdate

external_stylesheets=[dbc.icons.BOOTSTRAP, dbc.themes.BOOTSTRAP]

app = Dash(__name__, use_pages=True, pages_folder='', external_stylesheets=external_stylesheets, suppress_callback_exceptions=True)

dash.register_page('Home', path='/', icon="bi bi-house")
dash.register_page('scqcalert', path='/scqcalert', icon="bi bi-info-circle-fill")
dash.register_page('event', path='/event', icon="bi bi-globe")
dash.register_page('appDash', path='/appDash', icon="bi bi-phone-vibrate-fill")


ei = system.Environment.Instance()

external_stylesheets=[dbc.themes.BOOTSTRAP]
config = configparser.RawConfigParser()
config.read(ei.shareDir()+"/sceewv/apps.cfg")
cfg = dict(config.items('events'))
mapbox_access_token = open(cfg['mapboxtoken']).read()
extQuery = cfg['extquery']
extQuery = False if extQuery == 'False' else extQuery
extQuery = True if extQuery == 'True' else extQuery
fdsnwsName = cfg['fdsnwsname']
fdsnwsClient = cfg['fdsnwsclient']
agencia = cfg['agency']
deltaMin = int(cfg['deltatime'])
deltaDist = int(cfg['deltadist'])
validMagTypes = cfg['magtypes'].split(",")
latg_min = float(cfg['latmin'])
latg_max = float(cfg['latmax'])
long_min = float(cfg['lonmin'])
long_max = float(cfg['lonmax'])
latr_min = float(cfg['latrmin'])
latr_max = float(cfg['latrmax'])
lonr_min = float(cfg['lonrmin'])
lonr_max = float(cfg['lonrmax'])
json_path = cfg['jsonpath']
distInt = int(cfg['distint'])
fpStatus = cfg['fpstatus']
fpStatus = False if fpStatus == 'False' else fpStatus
fpStatus = True if fpStatus == 'True' else fpStatus
deltaDays = int(cfg['deltadays'])
minMag = float(cfg['minmag'])
maxMag = float(cfg['maxmag'])
cmaplat = float(cfg['cmaplat'])
cmaplon = float(cfg['cmaplon'])
zoomap = float(cfg['zoomap'])

tilerefurl = 'https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}'
tileurl = 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}'

png1 = ei.shareDir()+"/sceewv/assets/star.png"
star_base64 = base64.b64encode(open(png1, 'rb').read()).decode('ascii')
black_star = assign("""function(feature, latlng){
	const star = L.icon({iconUrl: 'data:image/png1;base64,"""+star_base64+"""',iconSize: [50, 50]});
	return L.marker(latlng, {icon: star});
	}""")

png12 = ei.shareDir()+"/sceewv/assets/yel_star.png"
star_base64 = base64.b64encode(open(png12, 'rb').read()).decode('ascii')
yel_star = assign("""function(feature, latlng){
	const star = L.icon({iconUrl: 'data:image/png12;base64,"""+star_base64+"""',iconSize: [50, 50]});
	return L.marker(latlng, {icon: star});
	}""")

png13 = ei.shareDir()+"/sceewv/assets/red_star.png"
star_base64 = base64.b64encode(open(png13, 'rb').read()).decode('ascii')
red_star = assign("""function(feature, latlng){
	const star = L.icon({iconUrl: 'data:image/png13;base64,"""+star_base64+"""',iconSize: [50, 50]});
	return L.marker(latlng, {icon: star});
	}""")

png1 = ei.shareDir()+"/sceewv/assets/star.png"
star_base64 = base64.b64encode(open(png1, 'rb').read()).decode('ascii')
black_star = assign("""function(feature, latlng){
	const star = L.icon({iconUrl: 'data:image/png1;base64,"""+star_base64+"""',iconSize: [50, 50]});
	return L.marker(latlng, {icon: star});
	}""")

png2 = ei.shareDir()+"/sceewv/assets/black_circle.png"
circ_base64 = base64.b64encode(open(png2, 'rb').read()).decode('ascii')
black_circ = assign("""function(feature, latlng){
	const circ = L.icon({iconUrl: 'data:image/png2;base64,"""+circ_base64+"""',iconSize: [30, 30]});
	return L.marker(latlng, {icon: circ, opacity: 0.70});
	}""")


png3 = ei.shareDir()+"/sceewv/assets/yel_circle.png"
circ_base64 = base64.b64encode(open(png3, 'rb').read()).decode('ascii')
yel_circ = assign("""function(feature, latlng){
	const circ = L.icon({iconUrl: 'data:image/png3;base64,"""+circ_base64+"""',iconSize: [30, 30]});
	return L.marker(latlng, {icon: circ, opacity: 0.70});
	}""")

png4 = ei.shareDir()+"/sceewv/assets/red_circle.png"
circ_base64 = base64.b64encode(open(png4, 'rb').read()).decode('ascii')
red_circ = assign("""function(feature, latlng){
	const circ = L.icon({iconUrl: 'data:image/png4;base64,"""+circ_base64+"""',iconSize: [30, 30]});
	return L.marker(latlng, {icon: circ, opacity: 0.70});
	}""")

sidebar = dbc.Nav(
	[
		dmc.Accordion(
		dmc.AccordionItem(
			[
			dbc.NavLink(
				[
					html.Div([
						html.I(className=page["icon"]),
						" ",
						page["name"]
					], className="ms-2"),
				],
				href=page["path"],
				active="exact",
				)
			for page in dash.page_registry.values()

			], value='Pages')
		)
	],
	vertical=True,
	pills=True,
	className="bg-light",
)

def super(string):
	superscript_map = {
		"0": "⁰", "1": "¹", "2": "²", "3": "³", "4": "⁴", "5": "⁵", "6": "⁶",
		"7": "⁷", "8": "⁸", "9": "⁹", "a": "ᵃ", "b": "ᵇ", "c": "ᶜ", "d": "ᵈ",
		"e": "ᵉ", "f": "ᶠ", "g": "ᵍ", "h": "ʰ", "i": "ᶦ", "j": "ʲ", "k": "ᵏ",
		"l": "ˡ", "m": "ᵐ", "n": "ⁿ", "o": "ᵒ", "p": "ᵖ", "q": "۹", "r": "ʳ",
		"s": "ˢ", "t": "ᵗ", "u": "ᵘ", "v": "ᵛ", "w": "ʷ", "x": "ˣ", "y": "ʸ",
		"z": "ᶻ", "A": "ᴬ", "B": "ᴮ", "C": "ᶜ", "D": "ᴰ", "E": "ᴱ", "F": "ᶠ",
		"G": "ᴳ", "H": "ᴴ", "I": "ᴵ", "J": "ᴶ", "K": "ᴷ", "L": "ᴸ", "M": "ᴹ",
		"N": "ᴺ", "O": "ᴼ", "P": "ᴾ", "Q": "Q", "R": "ᴿ", "S": "ˢ", "T": "ᵀ",
		"U": "ᵁ", "V": "ⱽ", "W": "ᵂ", "X": "ˣ", "Y": "ʸ", "Z": "ᶻ", "+": "⁺",
		"-": "⁻", "=": "⁼", "(": "⁽", ")": "⁾"}
	trans = str.maketrans(''.join(superscript_map.keys()),''.join(superscript_map.values()))
	return string.translate(trans)

def externalQuery(startTime,endTime,magValue,lat_min,lat_max,lon_min,lon_max):
	client = Client(fdsnwsClient)
	#client = Client(fdsnwsClient,debug=True)
	if magValue[1] < 9.5:
		maxMag = magValue[1]+0.5
	else:
		maxMag = magValue[1]
	cat = client.get_events(starttime=startTime,endtime=endTime,
	                        minmagnitude=magValue[0]-0.5,maxmagnitude=maxMag,
						    minlatitude=max([-90,lat_min-1]),maxlatitude=min([90,lat_max+1]),
						    minlongitude=max([-180,lon_min-1]),maxlongitude=min([180,lon_max+1]))	
	return cat

def compExt(EvalOri,extCat,prefOri):
	minute_delta = timedelta(minutes=deltaMin)
	degree_delta = deltaDist
	oriTime = UTCDateTime(prefOri["OriginTime"])
	oriLat = prefOri["latitude"]
	oriLon = prefOri["longitude"]
	for event in extCat.events:
		for origin in event.origins:
			extTime = origin.time
			deltaTime = timedelta(seconds=abs(extTime-oriTime))
			deltaLat = abs(origin.latitude - oriLat)
			deltaLon = abs(origin.longitude - oriLon)
			if deltaTime < minute_delta and deltaLat < degree_delta and deltaLon < degree_delta:
				EvalOri = 0
	return EvalOri

def ipe_allen2012_hyp(r, # hypocentral distance (km, float)
                      m, # magnitude (float)
                      a = 2.085,
                      b = 1.428,
                      c = -1.402,
                      d = 0.078,
                      s = 1,
                      m1=-0.209,
                      m2=2.042):
    rm = m1+m2*np.exp(m-5)
    I = a + b*m + c*np.log(np.sqrt(r**2 + rm**2))+s
    if r<50:
        I = a + b*m + c*np.log(np.sqrt(r**2 + rm**2))+d*np.log(r/50)+s
    return I # intensity (MMI, float)

def roman(num):
   res = ""
   table = [
      (10, "X"),
      (9, "IX"),
      (5, "V"),
      (4, "IV"),
      (1, "I")
   ]
   for cap, roman in table:
      d, m = divmod(num, cap)
      res += roman * d
      num = m
   if res == "":
      res == None
   return res

def prepare_data(startTime, endTime, magValue, reg_check):
#	dictMag = []
	dictPref = []
	dictPlot = []
	Tp_vect = []
	Fp_vect = []
	Fn_vect = []
	fmt = '%Y-%m-%d %H:%M:%S'
	if reg_check == ['reg']:
		lat_min=latr_min
		lat_max=latr_max
		lon_min=lonr_min
		lon_max=lonr_max
	else:
		lat_min=latg_min
		lat_max=latg_max
		lon_min=long_min
		lon_max=long_max
	if extQuery:
		try:
			extCat = externalQuery(startTime,endTime,magValue,lat_min,lat_max,lon_min,lon_max)
		except Exception as e:
			logging.debug("Empty external query or external catalog not found")
			logging.error("Error {}".format(e))
			extCat = read_events()
			extCat.clear()
	for root, dirs, files in os.walk(json_path):
		length = len(root)
		try:
			dayPath = datetime.datetime.strptime(root[-11:length],'/%Y/%m/%d')
			dayPath = dayPath.date()
		except Exception as e:
			logging.error("Error {}".format(e))
			continue
		if dayPath >= startTime and dayPath <= endTime:
			for item in files:
				# print(root)
				# print(item)
				checkExt = 0
				status = 1
				prefMagType = None
				prefMagVal = None
				EvalOri = None
				EvalMag = 1
				EvalEve = None
				mag = None
				regEval = 0
				magEval = 0
				fil = os.path.join(root, item)
				f = open(os.path.join(os.getcwd(), fil), 'r')
				creTime = []
				difOriTime = []
				dMag = []
				dPick = []
				tpAdd = []
				eewGMS = ""
				likelihoods = ""
				eewChecks = ""
				earlyMag = ""
				earlyDelay = ""
				delayEww = ""
				delayPtime = ""
				prefOriTime = None
				prefOriDepth = None
				data = json.load(f)
	#			dictMag.append(data)
				for iOri in range(len(data["origins"])):
					creTime.append(data["origins"][iOri]["CreationTime"])
					if data["prefOrigin"] == data["origins"][iOri]["ID"]:
						EvalOri = data["origins"][iOri]["EvaluationMode"]
						if EvalOri == 1 and extQuery:
							EvalOri = compExt(EvalOri,extCat,data["origins"][iOri])
							if EvalOri == 0:
								checkExt = 1
						lat = data["origins"][iOri]["latitude"]
						lon = data["origins"][iOri]["longitude"]
						if lat > lat_max or lat < lat_min or lon > lon_max or lon < lon_min:
							regEval = 1
							break
						prefOriTime = data["origins"][iOri]["OriginTime"]
						prefOriDepth = data["origins"][iOri]["depth"]
						data["origins"][iOri]["eventID"] = data["eventID"]
						prefOri = data["origins"][iOri]
	#					dictPref.append(data["origins"][iOri])
						try:
							for iPick in range(len(data["origins"][iOri]["picks"])):
								pickID = data["origins"][iOri]["picks"][iPick]["ID"]
								pCreaTime = data["origins"][iOri]["picks"][iPick]["CreationTime"]
								pTime = data["origins"][iOri]["picks"][iPick]["time"]
								dPick.append({"pickID":pickID,"pCreaTime":pCreaTime,"pTime":pTime})
						except Exception as e:
							logging.error("Error {}".format(e))
							pass
#							print("No pick in file %s origin %s" % (f,data["origins"][iOri]["ID"]))

					try:
						lat = data["origins"][iOri]["latitude"]
						lon = data["origins"][iOri]["longitude"]
						for iMag in range(len(data["origins"][iOri]["mags"])):
							likelihood = 0
							eewCheck = 0
							magTime = data["origins"][iOri]["mags"][iMag]["CreationTime"]
							magType = data["origins"][iOri]["mags"][iMag]["type"].split(' ')[0]
							magVal = data["origins"][iOri]["mags"][iMag]["value"]
							if magType in validMagTypes:
								EvalMag = 0
								if "comments" in data["origins"][iOri]["mags"][iMag]:
									for icomm in range(len(data["origins"][iOri]["mags"][iMag]["comments"])):
										commentID = data["origins"][iOri]["mags"][iMag]["comments"][icomm]["ID"]
										commentText = data["origins"][iOri]["mags"][iMag]["comments"][icomm]["text"]
										if commentID == "likelihood":
											likelihood = commentText
										elif commentID == "EEW":
											eewCheck = 1
								dMag.append({"magTime":magTime,"magType":magType, "magVal":magVal, "likelihood":likelihood, "eewCheck":eewCheck, "lat":lat,"lon":lon})
							if data["prefMag"] == data["origins"][iOri]["mags"][iMag]["ID"]:
								mag = data["origins"][iOri]["mags"][iMag]["value"]
								if mag < magValue[0] or mag > magValue[1]:
									magEval = 1
									break
								prefMagType = magType.split(' ')[0]
								prefMagVal = round(mag,2)
	#								dictPref[len(dictPref)-1]["Magnitude"] = "%s %s"%(magType,round(mag,2))
					except Exception as e:
						logging.error("Error {}".format(e))
						pass
#						print("No mag in file %s origin %s" % (fil,data["origins"][iOri]["ID"]))

				if regEval == 1 or magEval ==1 or (prefMagType == None and prefMagVal == None):
					continue
				else:
					pass

				if EvalOri == 0 and EvalMag == 0:
					EvalEve = "Tp"
					status = 1
					Tp_vect.append(prefOriTime)
				elif EvalOri == 1 and EvalMag == 0:
					EvalEve = "Fp"
					# status = None
					status = fpStatus
					Fp_vect.append(prefOriTime)
				elif EvalOri == 0 and EvalMag == 1:
					EvalEve = "Fn"
					status = None
					Fn_vect.append(prefOriTime)
				elif EvalOri == 1 and EvalMag == 1:
					continue

				dictPref.append(prefOri)
				dictPref[len(dictPref)-1]["Magnitude"] = "%s %s"%(prefMagType,prefMagVal)
				dictPref[len(dictPref)-1]["EvalEve"]=EvalEve
				dictPref[len(dictPref)-1]["checkExt"]=checkExt

				oTime = datetime.datetime.strptime(prefOriTime, fmt)

#				Add dif time first origin
#				for i in range(len(creTime)):
#					try:
#						cTime = datetime.datetime.strptime(creTime[i], fmt)
#						difOriTime.append((cTime-oTime).total_seconds())
#					except:
#						pass
##						print("No creation time in %s" % f)
#				try:
#					difTime = min(difOriTime)
#				except:
#					difTime = None
#				d = {"type":"1OriTime","difTime":difTime,"prefOriTime":prefOriTime}
#				dictPlot.append(d)

				for i in range(len(dPick)):
#					If want to use creation timne use dPick[i]["pCreaTime"] if you want to use pick time dPick[i]["pTime"]
					pTime = datetime.datetime.strptime(dPick[i]["pTime"], fmt)
					difpTime = (pTime-oTime).total_seconds()
					dPick[i]["difpTime"] = difpTime
				dPickSort = sorted(dPick, key=itemgetter("difpTime"))
				pIDvec = []
				count = 0
				for i in range(len(dPickSort)):
					if dPickSort[i]["pickID"] not in pIDvec:
						pIDvec.append(dPickSort[i]["pickID"])
						count += 1
					if count == 4:
						tp = "4th P"
						Ptime = dPickSort[i]["difpTime"]
						if status:
							# d = {"type":tp,"difTime":-Ptime,"prefOriTime":prefOriTime, "MMI":""}
							d = {"type":tp,"difTime":Ptime,"prefOriTime":prefOriTime, "MMI":""}
							dictPlot.append(d)
						break
				if EvalMag == 1:
					dictPref[len(dictPref)-1]["Early mag"]="None"
					dictPref[len(dictPref)-1]["Loc error [km]"]="None"
					dictPref[len(dictPref)-1]["EEW-GM MMI"]="None"
					dictPref[len(dictPref)-1]["Delay 4th P [s]"]="None"
					dictPref[len(dictPref)-1]["Delay EEW [s]"]="None"
					dictPref[len(dictPref)-1]["Likelihood"]="None"
					dictPref[len(dictPref)-1]["EEW"]= "None"
				else:
					tpDict = {}
					for i in range(len(dMag)):
						mTime = datetime.datetime.strptime(dMag[i]["magTime"], fmt)
						difMagTime = (mTime-oTime).total_seconds()
						dMag[i]["difMagTime"] = difMagTime
						tp = dMag[i]["magType"]
						if tp in tpDict:
							tpDict[tp].append(dMag[i]["eewCheck"])
						else:
							tpDict[tp] = [dMag[i]["eewCheck"]]
					dMagSort = sorted(dMag, key=itemgetter("difMagTime"))
					for i in range(len(dMagSort)):
						tp = dMagSort[i]["magType"]
						difTime = dMagSort[i]["difMagTime"]
						magVal = dMagSort[i]["magVal"]
						cond = 0
						if tp not in tpAdd and tp in validMagTypes:
							if 1 in tpDict[tp]:
								if dMagSort[i]["eewCheck"] == 1:
									cond = 1
							else:
								cond = 1
							if cond == 1:
								if earlyMag != "":
									earlyMag += "\n"
									delayEww += "\n"
									delayPtime += "\n"
									eewGMS += "\n"
									likelihoods += "\n"
									eewChecks += "\n"
								tpAdd.append(tp)
								#eewGM = ipe_allen2012_hyp((max([0.01, (difMagTime*3.3)**2-prefOriDepth**2]))**.5,magVal)
								eewGM = ipe_allen2012_hyp(distInt,magVal)
								if eewGM > 0:
									eewGM = roman(round(eewGM))
								else:
									eewGM = roman(round(0))
								# if difTime-Ptime <= 0:
									# eewTime = 0
								# elif difTime-Ptime > 50:
									# eewTime = float('nan')
								# else:
									# eewTime = difTime-Ptime
								if difTime > 50:
									eewTime = float('nan')
								else:
									eewTime = difTime
								if status:
									d = {"type":tp,"difTime":eewTime,"prefOriTime":prefOriTime, "MMI":eewGM}
									dictPlot.append(d)
								likelihood = str(round(float(dMagSort[i]["likelihood"]),2))
								earlyMag += tp+" "+str(round(magVal,2))
								delayEww += str(eewTime)
								delayPtime += str(Ptime)
								eewGMS += str(eewGM)
								likelihoods += likelihood
								eewCheck = dMagSort[i]["eewCheck"]
								if eewCheck == 1:
									eewCheck = "✅"
								else:
									eewCheck = "❌"
								eewChecks += eewCheck
								locPref = np.array((float(prefOri["latitude"]),float(prefOri["longitude"])))
								locOri = np.array((float(dMagSort[i]["lat"]),float(dMagSort[i]["lon"])))
								locErr = str(int(np.linalg.norm(locPref-locOri)*111.1))
#								r=10 # Ojo distancia utilizada para calcular EEW-GM
					dictPref[len(dictPref)-1]["Early mag"]=earlyMag
					dictPref[len(dictPref)-1]["Loc error [km]"]=locErr
					dictPref[len(dictPref)-1]["EEW-GM MMI"]=eewGMS
					dictPref[len(dictPref)-1]["Delay 4th P [s]"]=delayPtime
					dictPref[len(dictPref)-1]["Delay EEW [s]"]=delayEww
					dictPref[len(dictPref)-1]["Likelihood"]=likelihoods
					dictPref[len(dictPref)-1]["EEW"]=eewChecks

#	data_origins = json_normalize(dicts, record_path=['origins'],meta=['eventID'])
#	dataMag = json_normalize(dictMag, record_path=['origins','mags'],meta=['eventID', ['origins','CreationTime'],['origins','longitude'],['origins','latitude'],['origins','OriginTime'],['origins','ID']])
	dataPref = json_normalize(dictPref).drop(['CreationTime','mags','picks','ID'], axis='columns')
	dataPlot = json_normalize(dictPlot)
	dataPref.dropna(inplace=True)
	dataPref.reset_index(drop=True, inplace=True)
	dataPlot.dropna(inplace=True)
	dataPlot.reset_index(drop=True, inplace=True)
	dataPref['id']=dataPref['eventID']
	dataPref.set_index('id', inplace=True, drop=False)

#	print(dataPref)
#	data = data_origins.merge(data_mags, left_on="ID", right_on='origins.ID').drop('mags', axis='columns')
#	data_origins.to_csv("data_origins.csv")
#	data_mags.to_csv("data_mags.csv")
#	data.to_csv("data.csv")
	return dataPref, dataPlot, Fp_vect, Fn_vect, Tp_vect

# today = date.today()
# endTime = today
# startTime = endTime - timedelta(days=deltaDays)
# magValue = [minMag, maxMag]
# reg_check = []

# dataPref,dataPlot,Fp_vect,Fn_vect,Tp_vect = prepare_data(startTime, endTime, magValue, reg_check)

initial_active_cell = {"row": 0, "column": 0, 'column_id': 'OriginTime'}
colors = ["royalblue","crimson","lightseagreen","orange","lightgrey"]
tableColumns = ["OriginTime","Magnitude","Early mag","Loc error [km]","Delay 4th P [s]","Delay EEW [s]","EEW-GM MMI","Likelihood","EEW","eventID"]

# tab1_content = dcc.Graph(id='mag',config= {'displaylogo': False},style = {'width':'98vw','margin-left': "0.5vw",'height':'32vh'},responsive = True)
tab1_content = dcc.Graph(id='mag',config= {'displaylogo': False},style = {'width':'98.75vw','margin-left': "0.5vw",'height':'35vh'},responsive = True)
tab2_content = dbc.Row(
	[
	dbc.Col([
		dcc.Graph(id='histogram',config= {'displaylogo': False},style = {'width':'49vw','margin-left': "0.5vw",'height':'35vh'},responsive = True)
			]),
	dbc.Col([
		dcc.Graph(id='pie',config= {'displaylogo': False},style = {'width':'49.5vw','margin-left': "0.0vw",'height':'35vh'},responsive = True)
			])
	],className="g-0"),

layout = html.Div(
	[
	html.Div(id='reload'),
	html.Div(id='pageContent'),
	dbc.Row(
		[
			dbc.Col(
				id = 'container_side_menu',
				width=1),
			dbc.Col(
				[
				html.Div(id = 'eventsText',
					# style={"margin-left": "10%","display": "inline-block",}
					)
				],width=10)]
		),
	dbc.Row(
		[
		dbc.Col(
			[
### map ###
			dbc.Row(
				[
				dbc.Col(
					id = 'container_left',
					),
				],className="g-0"),
			]),

### table ###
			dbc.Col(
				# id = 'container_r',
				# ),
				dash_table.DataTable(
#					style_data={'lineHeight': '15px','whiteSpace': 'normal','height': 'auto'},
					id = 'tbl',
					# data = dataPref.drop(['longitude','latitude'], axis='columns').to_dict('records'),
					sort_action='native',
					columns = [{'name': i, 'id': i} for i in tableColumns],
					export_format="csv",
#					editable=True,
					style_as_list_view=True,
					row_selectable="single",
#					fixed_rows={'headers': True},
#					page_size=20,
					page_action='none',
					# style_table={'height':'400px', 'overflowY': 'auto'},
					style_table = {'height': '55vh','width': '49.5vw', "display": "block", "position": "relative",'overflowY': 'auto'},
					# style_table={'overflowY': 'auto'},
					# style_data={'whiteSpace': 'normal','height': 'auto'},
					selected_rows=[],
#					fill_width=True,
					style_cell={'minWidth': '5px', 'width': '10px', 'maxWidth': '180px','textAlign': 'center','whiteSpace': 'pre-line'},
					style_header={
						'backgroundColor': 'rgb(210, 210, 210)',
						'fontWeight': 'bold',
						'color': 'black',
						'textAlign': 'left'
						},
					active_cell=initial_active_cell
					),
				)
		],className="g-0"),
	dbc.Row(
		[
### plots ###
			dbc.Col(
				[
				# id = 'container_b',
				# width=12,
				dbc.Tabs(
					[
						# dbc.Tab(label="History", tab_id="tab-1", tab_style={"marginLeft": "auto"}),
						dbc.Tab(label="History", tab_id="tab-1",tab_class_name="flex-grow-1 text-center"),
						dbc.Tab(label="Summary", tab_id="tab-2",tab_class_name="flex-grow-1 text-center"),
					],
					id="tabsEvents",
					active_tab="tab-1",
					style = {'width':'99vw','margin-left': "0.5vw"},
					),
					html.Div(id="content"),
				]
				),
		],className="g-0"),
	# dcc.Interval(
		# id='interval-component',
		# interval=1*1000, # in milliseconds
		# n_intervals=0)
	])

@callback(
	Output('container_side_menu', 'children'),
	Input("reload", "children"))
def side_menu(aux):
	today = date.today()
	endTime = today
	startTime = endTime - timedelta(days=deltaDays)
	magValue = [minMag, maxMag]
	return html.Div([
			dbc.Button("Menu", id="open-canvas", n_clicks=0, style={"margin-left": "8%"}),
			dbc.Offcanvas([
				sidebar,
				html.Hr(style={'borderWidth': "1vh", "width": "100%", "color": "dimgrey"}),
				html.P('Select Time Range:'),
				dcc.DatePickerRange(
					id='date-range',
					minimum_nights=1,
					clearable=True,
					with_portal=True,
					start_date=startTime,
					end_date=endTime,
					calendar_orientation='vertical',
#							min_date_allowed=date(1995, 8, 5),
#							max_date_allowed=date(2017, 9, 19),
#							initial_visible_month=date(2017, 8, 5),
					style={'width': '100%', 'display': 'inline-block'}
					),
				html.Div([
				dbc.Button('Query DB using Time Range',id='re_db', n_clicks=0,  style = { 'width' : '40%', 'margin-top': '15vw', "margin":"15px" ,'border-radius': '8px'}),
				dcc.Loading(id='loading', type="default")
				]),
				# dbc.Spinner(children = dbc.Button(id='re_db'), id='loading'),
				# html.Button('Query DB using Time Range',id='re_db', n_clicks=0,  style = { 'width' : '40%', 'margin-top': '15vw', "margin":"15px" ,'border-radius': '8px'}),
				# dcc.Loading(id="loading-db", type="default", children=html.Div(id="loading-db-out")),
				html.P('Select Magnitude Range:'),
				dcc.RangeSlider(
					id='mag-range',
					min=0,
					max=10,
#							step=1,
					value=magValue,
					tooltip={"placement": "bottom", "always_visible": True}
					),
				html.P('Region:'),
					dcc.Checklist(
						options={
							'reg': ' Region '+agencia,
							},
						value = [],
						id='reg-check'
					),
				],
				id="canvas",
				title="Menu:",
				),
		####dcc.Store stores the intermediate value
			dcc.Store(id='query-data'),
			]),

# @callback(Output("loading", "children"), Input("re_db", "n_clicks"))
# def input_triggers_spinner(n):
    # if n==0:
        # raise PreventUpdate
    # else:
        # time.sleep(1)
    # return


@callback(
	Output('reload', 'data'),
	Output('loading', 'parent_style'),
	Input('re_db','n_clicks'),
	State('date-range', 'start_date'),
	State('date-range', 'end_date'))
def updateEvents(n_clicks, start_date, end_date):
	new_loading_style = {'position': 'absolute', 'align-self': 'center'}
	if not n_clicks:
		raise PreventUpdate
	command = 'sceewv -u loadDB --query --start "'+start_date+' 00:00:00" --end "'+end_date+' 23:59:59" --debug'
	subprocess.run(command, shell = True, executable="/bin/bash", timeout=300)
	# os.system('sceewv -u loadDB --query --start "'+start_date+' 00:00:00" --end "'+end_date+' 23:59:59" --debug')
	data = True
	return data, new_loading_style

@callback(
	Output('query-data', 'data'),
	Input("date-range", 'start_date'),
	Input('date-range', 'end_date'),
	Input("mag-range", 'value'),
	Input("reg-check", "value"),
	Input("tbl", "active_cell"))
def query_data(start_date, end_date, magValue, reg_check,active_cell):
	s = None
	startTime = datetime.datetime.strptime(start_date, '%Y-%m-%d')
	startTime = startTime.date()
	endTime = datetime.datetime.strptime(end_date, '%Y-%m-%d')
	endTime = endTime.date()
	try:
		dataPref,dataPlot,Fp_vect,Fn_vect,Tp_vect = prepare_data(startTime, endTime, magValue, reg_check)
	except Exception as e:
		logging.debug("No data in selected period")
		logging.error("Error {}".format(e))
	if active_cell:
		for i in range(len(dataPref['Magnitude'])):
			evalEve = dataPref['EvalEve'][i]
			if 'row_id' in active_cell:
				if dataPref['eventID'][i] == active_cell['row_id']:
					if evalEve == "Fn":
						s = red_star
					elif evalEve == "Fp":
						s = yel_star
					elif evalEve == "Tp":
						s = black_star
	datasets = {
		'dataPref': dataPref.to_json(orient='split', date_format='iso'),
		'dataPlot': dataPlot.to_json(orient='split', date_format='iso'),
		'Fp_vect': Fp_vect,
		'Fn_vect': Fn_vect,
		'Tp_vect': Tp_vect,
		'star': s
		}
	return json.dumps(datasets)

@callback(
	Output('eventsText', 'children'),
	Input("date-range", 'start_date'),
	Input('date-range', 'end_date'),
	Input("mag-range", 'value'),
	Input("reg-check", "value"))
def updateT(start_date, end_date, magValue, reg_check):
	if reg_check == 'reg':
		reg = "YES"
	else:
		reg = "NO"
	content = [
				html.H1(children = ["Start Day: %s "%start_date,
					"       End Day: %s "%end_date,
					"       Magnitude Range: %s "%magValue,
					"       Filter by Region: %s "%reg,
					],
				style = {
					'textAlign' : 'center',
					# 'color' : '#494949',
					'color' : 'white',
					"font-weight": "bold",
					# "size":"14px",
					"font-size": "20px",
					'whiteSpace': 'pre-wrap',
					# "font-family": 'Gill Sans',
					'backgroundColor':'dimgrey',
					}
					),
				# html.Div("This is the space for event Info"),
			]
	return content

@callback(
	Output("canvas", "is_open"),
	Input("open-canvas", "n_clicks"),
	State("canvas", "is_open")
	)
def toggle_offcanvas(n1, is_open):
	if n1:
		return not is_open
	return is_open

@callback(
	Output('container_left', 'children'),
	Input("query-data", 'data'))
def update_map(data):
	datasets = json.loads(data)
	if datasets['star'] is not None:
		star = datasets['star']
	else:
		star = black_star
	# mapText="<b>Events chronological and geographical distribution from %s to %s </b><br> Agency: <b>%s</b>. Reference agency: <b>%s</b> and <b>%s</b>. <br> Dashboard creation time:%s"%(start_date,end_date,agencia,agencia,fdsnwsName,today)
	content = dl.Map(children=
		[
		dl.TileLayer(url=tileurl, maxZoom=13),
		dl.TileLayer(url=tilerefurl, maxZoom=13),
		dl.GeoJSON(
			options=dict(pointToLayer=black_circ),
			id='tporis'),
		dl.GeoJSON(
			options=dict(pointToLayer=yel_circ),
			id='fporis'),
		dl.GeoJSON(
			options=dict(pointToLayer=red_circ),
			id='fnoris'),
		dl.GeoJSON(
			options=dict(pointToLayer=star),
			id='origin'),
		],
		style = {'width': '49vw', 'height': '55vh', 'margin-left': "0.5vw", "display": "block", "position": "relative"},
		id='leafmap',
		center=[13.5, -88.5],
		zoom=8)
	return content

@callback(
	Output('origin', 'data'),
	Input("tbl", "active_cell"),
	Input('query-data', 'data'))
def orimarker(active_cell, data):
	if active_cell:
		datasets = json.loads(data)
		dataPref = pd.read_json(datasets['dataPref'], orient='split')
		for i in range(len(dataPref['Magnitude'])):
			if 'row_id' in active_cell:
				if dataPref['eventID'][i] == active_cell['row_id']:
					lon = dataPref['longitude'][i]
					lat = dataPref['latitude'][i]
					iD = dataPref['eventID'][i]
					mag = dataPref['Magnitude'][i]
					time = dataPref['OriginTime'][i]
		oriInfo = str(mag)+"  "+str(time)+" "+str(iD)
		locs=[{'lat': lat, 'lon': lon, 'info': oriInfo}]
		geojson = dlx.dicts_to_geojson([{**c, **dict(tooltip=c['info'])} for c in locs])
		return geojson

@callback(
	Output('tporis', 'data'),
	Input('query-data', 'data'))
def orimarker(data):
	datasets = json.loads(data)
	dataPref = pd.read_json(datasets['dataPref'], orient='split')
	locs = []
	for i in range(len(dataPref['Magnitude'])):
		evalEve = dataPref['EvalEve'][i]
		if evalEve == "Tp":
			star = black_star
			lon = dataPref['longitude'][i]
			lat = dataPref['latitude'][i]
			oriInfo = dataPref['Magnitude'][i]+"  "+dataPref['OriginTime'][i]+" "+dataPref['eventID'][i]
			dOris = {'lat': lat, 'lon': lon, 'info': oriInfo}
			locs.append(dOris)
	geojson = dlx.dicts_to_geojson([{**c, **dict(tooltip=c['info'])} for c in locs])
	return geojson

@callback(
	Output('fporis', 'data'),
	Input('query-data', 'data'))
def orimarker(data):
	datasets = json.loads(data)
	dataPref = pd.read_json(datasets['dataPref'], orient='split')
	locs = []
	for i in range(len(dataPref['Magnitude'])):
		evalEve = dataPref['EvalEve'][i]
		if evalEve == "Fp":
			star = yel_star
			lon = dataPref['longitude'][i]
			lat = dataPref['latitude'][i]
			oriInfo = dataPref['Magnitude'][i]+"  "+dataPref['OriginTime'][i]+" "+dataPref['eventID'][i]
			dOris = {'lat': lat, 'lon': lon, 'info': oriInfo}
			locs.append(dOris)
	geojson = dlx.dicts_to_geojson([{**c, **dict(tooltip=c['info'])} for c in locs])
	return geojson

@callback(
	Output('fnoris', 'data'),
	Input('query-data', 'data'))
def orimarker(data):
	datasets = json.loads(data)
	dataPref = pd.read_json(datasets['dataPref'], orient='split')
	locs = []
	for i in range(len(dataPref['Magnitude'])):
		evalEve = dataPref['EvalEve'][i]
		if evalEve == "Fn":
			star = red_star
			lon = dataPref['longitude'][i]
			lat = dataPref['latitude'][i]
			oriInfo = dataPref['Magnitude'][i]+"  "+dataPref['OriginTime'][i]+" "+dataPref['eventID'][i]
			dOris = {'lat': lat, 'lon': lon, 'info': oriInfo}
			locs.append(dOris)
	geojson = dlx.dicts_to_geojson([{**c, **dict(tooltip=c['info'])} for c in locs])
	return geojson

@callback(Output("storeEventID", "data"),
		[Input('tbl', "derived_virtual_data"),
		Input('tbl', 'derived_virtual_selected_rows')])
def goEvent(data, selected_rows):
	if selected_rows:
		selected_data=data[selected_rows[0]]
		eventID = selected_data["eventID"]
		return eventID

@callback(Output("content", "children"), [Input("tabsEvents", "active_tab")])
def switch_tab(at):
    if at == "tab-1":
        return tab1_content
    elif at == "tab-2":
        return tab2_content
    return html.P("This shouldn't ever be displayed...")

@callback(
    Output('tbl', 'style_data_conditional'),
    Input('tbl', 'active_cell'),
    Input('date-range', 'start_date')
)
def select_row(active_cell,start_date):
    return [{
        'if': { 'row_index': active_cell["row"] },
        'background_color': '#D2F3FF'},
          {
              'if': {
                'column_id': ['OriginTime', 'Magnitude'],
                'filter_query': '{EvalEve} = Fn'
              },
             'backgroundColor': '#ff9c9c',
             'color': 'white'
          },
          {
              'if': {
                'column_id': ['OriginTime', 'Magnitude'],
                'filter_query': '{EvalEve} = Fp'
              },
             'backgroundColor': '#fadc9b',
             'color': 'white'
          },
    ]

@callback(
	Output('tbl', 'active_cell'),
	Input("tporis", "click_feature"),
	Input("fporis", "click_feature"),
	Input("fnoris", "click_feature"),
	Input("origin", "click_feature"),
	Input('tbl', 'derived_virtual_data'),
	Input("tbl", "active_cell")
	)
def update_table(c_tporis,c_fporis,c_fnoris,c_origin,tblData,old_active_cell):
	if c_tporis is not None:
		click = c_tporis
	elif c_fporis is not None:
		click = c_fporis
	elif c_fnoris is not None:
		click = c_fnoris
	elif c_origin is not None:
		click = c_origin
	try:
		idSelected = click['properties']['info'].split(" ")[5]
		for i in range(len(tblData)):
			if tblData[i]["eventID"] == idSelected:
				active_cell = {'row':i,'column':0,'column_id':'OriginTime','row_id':idSelected}
				return active_cell
	except Exception as e:
		logging.error("Error {}".format(e))
		# active_cell = {'row':0,'column':0,'column_id':'OriginTime','row_id':tblData[0]["eventID"]}
		active_cell = old_active_cell
		return active_cell

@callback(
	Output('tbl', 'data'),
	Input('query-data', 'data'))
def update_table(data):
	datasets = json.loads(data)
	dataPref = pd.read_json(datasets['dataPref'], orient='split')
	d = dataPref.drop(['longitude','latitude'], axis='columns')
	for index, row in d.iterrows():
		if row["checkExt"] == 1:
			magMod = str(row["Magnitude"])+str(super(fdsnwsName))
			# magMod = str(row["Magnitude"])+'[%s](https://earthquake.usgs.gov/earthquakes/map/)'%fdsnwsName
			d.at[index, "Magnitude"] = magMod
	return d.to_dict('records')

def create_time_series(df,Fp_vect,Fn_vect,title):
	xname = "UTC DateTime"
	yname = "Seconds After Origin Time"
	df["symbol"]="triangle-up"
	for index, row in df.iterrows():
		if row["type"] == "4th P":
			df.at[index, "symbol"] = "triangle-up"
		else:
			# df.at[index, "symbol"] = "triangle-down"
			df.at[index, "symbol"] = "triangle-up"
	# fig = px.scatter(df, x='prefOriTime', y='difTime', color='type',symbol='symbol',symbol_map='identity',template="presentation")
	fig = px.scatter(df, x='prefOriTime', y='difTime', color='type',symbol='symbol',symbol_map='identity',template="ggplot2")
	dmin = df["difTime"].values.min()
	dmax = df["difTime"].values.max()
	# div = dmax/abs(dmin)
	# step = abs(dmin)/(5-round(div))
	# tickvals1 = np.arange(dmin,0,step)
	# ticktext1 = np.arange(-dmin,0,-step)
	# tickvals2 = np.arange(0,dmax,step)
	# ticktext = np.concatenate((ticktext1.astype(int), tickvals2.astype(int)))
	# tickvals = np.concatenate((tickvals1.astype(int), tickvals2.astype(int)))
	dfg = df.groupby("prefOriTime")
	dfg = [dfg.get_group(x) for x in dfg.groups]
	for dfO in dfg:
		time = dfO["prefOriTime"].values[0]
		pts = dfO["difTime"].values
		try:
			# fig.add_trace(go.Scatter(x=[time,time],y=[pts[0],pts[1]],mode='lines',line=dict(color="black", width=1),showlegend=False))
			fig.add_trace(go.Scatter(x=[time,time],y=[0,pts[1]],mode='lines',line=dict(color="black", width=1),showlegend=False))
			fig.add_trace(go.Scatter(x=[time,time],y=[0,pts[0]],mode='lines',line=dict(color="black", width=1),showlegend=False))
		except Exception as e:
			logging.error("Error {}".format(e))
			pass
	for xi in Fp_vect:
		if xi == Fp_vect[len(Fp_vect)-1]:
			fig.add_trace(go.Scatter(x=[xi,xi],y=[0,dmax],mode='lines',line=dict(color="#fadc9b", width=2, dash='dash'), name='suspected'))
		else:
			fig.add_trace(go.Scatter(x=[xi,xi],y=[0,dmax],mode='lines',line=dict(color="#fadc9b", width=2, dash='dash'),showlegend=False))
	for xi in Fn_vect:
		if xi == Fn_vect[len(Fn_vect)-1]:
			fig.add_trace(go.Scatter(x=[xi,xi],y=[0,dmax],mode='lines',line=dict(color="#ff9c9c", width=2, dash='dash'), name='False -'))
		else:
			fig.add_trace(go.Scatter(x=[xi,xi],y=[0,dmax],mode='lines',line=dict(color="#ff9c9c", width=2, dash='dash'),showlegend=False))
	fig.update_traces(marker=dict(size=10))
	# fig.update_xaxes(showgrid=False)
	config = {'displaylogo': False}
	fig.update_layout(
		margin={'r': 0,'l':0,'t':0,'b':0},
		# xaxis=dict(showgrid=False, linecolor="dimgrey", linewidth=2, zeroline=False,title=None,ticklabelposition="outside right"),
		# yaxis=dict(showgrid=False, linecolor="dimgrey", linewidth=2, zeroline=True,title=None,ticklabelposition="inside top",tickmode="array",ticktext=ticktext,tickvals=tickvals),
		legend=dict(title="True +:"),
		# legend=dict(bordercolor="dimgrey",borderwidth=1,x=1.0,y=0.93),
		modebar_add=['drawline',
			'drawopenpath',
			'drawclosedpath',
			'drawcircle',
			'drawrect',
			'eraseshape']
		)
	fig.update_xaxes(minor_ticks="inside", minor=dict(showgrid=True), showline=True, linewidth=2, linecolor='dimgrey', title_text=xname)
	fig.update_yaxes(minor_ticks="inside", minor=dict(showgrid=True), showline=True, linewidth=2, linecolor='dimgrey', title_text=yname)
	return fig

def create_histo(dataPlot):
	xname = "Delay computing EEW magnitude [s]"
	yname = "Density"
	delayAll = []
	group_labels = []
	data = dataPlot.groupby(['MMI'])
	data = [data.get_group(x) for x in data.groups]
	for df in data:
		delay = []
		if df['MMI'].unique()[0] != "":
			group_labels.append(df['MMI'].unique()[0])
		for index, row in df.iterrows():
			if row["type"] in validMagTypes:
				delay.append(row["difTime"])
		if delay != []:
			delayAll.append(delay)
	fig = ff.create_distplot(delayAll, group_labels, curve_type='normal', show_hist=False, show_rug=False)
	fig.update_layout(
		margin={'r': 0,'l':0,'t':0,'b':0},
		legend=dict(title="Intensity values:")
		)
	fig.update_xaxes(minor_ticks="inside", minor=dict(showgrid=True), showline=True, linewidth=2, linecolor='dimgrey', title_text=xname)
	fig.update_yaxes(minor_ticks="inside", minor=dict(showgrid=True), showline=True, linewidth=2, linecolor='dimgrey', title_text=yname)
	return fig

@callback(
	Output('histogram', 'figure'),
	Input('query-data', 'data'))
def update_graph(data):
	datasets = json.loads(data)
	dataPlot = pd.read_json(datasets['dataPlot'], orient='split')
	return create_histo(dataPlot)

def create_pie(Tp_vect,Fp_vect,Fn_vect):
#	colors = ['blue','blue','blue','#fadc9b','#fadc9b','#fadc9b','#ff9c9c']
	subeval = ["VS","FD","VS&FD","VS","FD","VS&FD",None]
	eval_types = ["T+","T+","T+","F+","F+","F+","F-"]
	eve_conunt = [len(Tp_vect)/3,len(Tp_vect)/3,len(Tp_vect)/3,len(Fp_vect)/3,len(Fp_vect)/3,len(Fp_vect)/3,len(Fn_vect)]
	df = pd.DataFrame(dict(eval_types=eval_types,subeval=subeval,eve_conunt=eve_conunt))
	fig = px.sunburst(df, path=['eval_types','subeval'], values='eve_conunt', color='eval_types',color_discrete_map={'(?)':'black','T+':'blue', 'F+':'#fadc9b', 'F-':'#ff9c9c'})
	fig.update_layout(
		margin={'r': 0,'l':0,'t':0,'b':0},
		)
	return fig

@callback(
	Output('pie', 'figure'),
	Input('query-data', 'data'))
def update_graph(data):
	datasets = json.loads(data)
	Fp_vect = datasets['Fp_vect']
	Fn_vect = datasets['Fn_vect']
	Tp_vect = datasets['Fn_vect']
	return create_pie(Tp_vect, Fp_vect, Fn_vect)

@callback(
	Output('mag', 'figure'),
	Input('query-data', 'data'))
def update_graph(data):
	datasets = json.loads(data)
	dataPref = pd.read_json(datasets['dataPref'], orient='split')
	dataPlot = pd.read_json(datasets['dataPlot'], orient='split')
	Fp_vect = datasets['Fp_vect']
	Fn_vect = datasets['Fn_vect']
	title = '<b>TimeSpan</b>'
	return create_time_series(dataPlot.drop(['MMI'], axis=1), Fp_vect, Fn_vect, title)
