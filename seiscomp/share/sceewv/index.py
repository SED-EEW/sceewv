#!/usr/bin/env python
"""
Copyright (C) by ETHZ/SED

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

Author of the Software: Camilo Munoz
"""

from dash import Dash, dcc, html, Input, Output, callback
from seiscomp import system, logging
import dash_bootstrap_components as dbc
# try:
import events_dash
import event_dash
# except Exception as e:
	# logging.debug('No event(s) dashboards')
	# logging.error("Error {}".format(e))
import sys
import configparser

ei = system.Environment.Instance()


external_stylesheets=[dbc.themes.BOOTSTRAP]
config = configparser.RawConfigParser()
config.read(ei.shareDir()+"/sceewv/apps.cfg")
cfg = dict(config.items('index'))
alert = cfg['alert']
alert = False if alert == 'False' else alert
alert = True if alert == 'True' else alert
if alert:
	alertPath = cfg['alertpath']
	sys.path.append(alertPath)
	import dashboard

appdash = cfg['appdash']
appdash = False if appdash == 'False' else appdash
appdash = True if appdash == 'True' else appdash
if appdash:
	appdashPath = cfg['appdashpath']
	sys.path.append(appdashPath)
	import main

host = cfg['host']
port = cfg['port']

external_stylesheets=[dbc.icons.BOOTSTRAP, dbc.themes.BOOTSTRAP]

app = Dash(__name__, external_stylesheets=external_stylesheets, suppress_callback_exceptions=True)
server = app.server

# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "20rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

def serve_layout():
	return html.Div([
		dcc.Location(id='url', refresh=False),
		html.Div(id='page-content'),
		dcc.Store(id="storeEventID", data=None),
		])

app.layout = serve_layout


index_page = html.Div(
    [
        html.H2("ATTAC Dashboards", className="display-4"),
        html.Hr(),
        html.P(
            "Please select one of the available dashboards", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Go to scqcalert", href="/scqcalert", active="exact"),
                dbc.NavLink("Go to event", href="/event", active="exact"),
                dbc.NavLink("Go to events", href="/events", active="exact"),
                dbc.NavLink("Go to appDash", href="/appDash", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

# index_page = html.Div([
    # dcc.Link('Go to scqcalert', href='/scqcalert'),
    # html.Br(),
    # dcc.Link('Go to event', href='/event'),
    # html.Br(),
    # dcc.Link('Go to events', href='/events'),
    # html.Br(),
    # dcc.Link('Go to appDash', href='/appDash'),
# ])

@callback(Output('page-content', 'children'),
              Input('url', 'pathname'))
def display_page(pathname):
	if pathname == '/scqcalert':
		return dashboard.scqcalert_layout
	elif pathname == '/event':
		return event_dash.layout
	elif pathname == '/events':
		return events_dash.layout
	elif pathname == '/appDash':
		return main.layout
	else:
		return index_page

if __name__ == '__main__':
#    app.run_server(debug=True)
    app.run(debug=False,
            host=host,
            port=port,
            dev_tools_ui=False,
            dev_tools_props_check=False,
            dev_tools_serve_dev_bundles=False,
            dev_tools_hot_reload=False,
            dev_tools_hot_reload_interval=False,
            dev_tools_hot_reload_watch_interval=False,
            dev_tools_hot_reload_max_retry=False,
            dev_tools_silence_routes_logging=False,
            dev_tools_prune_errors=False,
            )
