# sceewv

## About

sceewv is a SeisComP module to review and analyze the performance of 
an Early Warning system. This module has been developed by a partnership 
between Swiss Seismological Service (SED) and Seismological Centers in 
Central America (OVSICORI, INSIVUMEH, MARN, and INETER) during the ATTAC 
project which is an effort to implement and test an Early Warning system 
in countries of Central America.

### Prerequisites

The following packages should be installed to run sceewv:

- Python V3+
- SeisComP V4+
- SED-EEW addons (scvsmag, scfinder)
- Dash V2+
- dash_bootstrap_components
- pandas
- geopandas
- pandas-read-xml
- svgpath2mpl
- python3-scipy
- obspy
- configparser

### Installation

1. Clone the repository https://gitlab.seismo.ethz.ch/SED-EEW/sceewv
2. Copy all content inside seiscomp/bin, seiscomp/etc, seiscomp/share 
   directories in your $SEISCOMP_ROOT path
3. Test it running ```seiscomp start sceewv```

### Running Dashboards

After install sceewv go to $SEISCOMP_ROOT/share/sceewv and run ```python index.py```
We recommend adding a crontab line to be sure the dashboard is running:
```PYTHONPATH=$SEISCOMP_ROOT/lib/python LD_LIBRARY_PATH=$SEISCOMP_ROOT/lib python $SEISCOMP_ROOT/share/sceewv/index.py > YOUR_LOG_PATH/sceewv_dash.log 2>&1 & ```

### Configuration

Please use scconfig or sceewv.cfg file inside $SEISCOMP_ROOT/etc folder to configure sceewv module and the dashboard.
To check parameter descriptions please go to scconfig and look for sceewv in the left panel or check $SEISCOMP_ROOT/etc/descriptions/sceewv.xml file.

### Usage

Please use the sceewv help to check options to run the module ```sceewv --help```

### Documentation

Please see detailed documentation in Read the Docs: https://seiscomp-eew-qc-view.readthedocs.io/en/main/
