FROM ghcr.io/sed-eew/sed-eew-seiscomp-contributions:latest
#FROM amd64/debian:stable-slim

MAINTAINER Fred Massin  <fmassin@sed.ethz.ch>

ENV    WORK_DIR /usr/local/src
ENV INSTALL_DIR /opt/seiscomp

# Fix Debian  env
ENV DEBIAN_FRONTEND noninteractive
ENV INITRD No
ENV FAKE_CHROOT 1

# Setup sysop's user and group id
ENV USER_ID 1000
ENV GROUP_ID 1000

WORKDIR $WORK_DIR

RUN echo 'force-unsafe-io' | tee /etc/dpkg/dpkg.cfg.d/02apt-speedup \
    && echo 'DPkg::Post-Invoke {"/bin/rm -f /var/cache/apt/archives/*.deb || true";};' | tee /etc/apt/apt.conf.d/no-cache \
    && apt-get update \
    && apt-get dist-upgrade -y --no-install-recommends 
     
RUN apt-get update && \
    apt-get install -y \
        openssh-server \
        openssl \
        libssl-dev \
        net-tools \
        python3 \
        python3-pip \
        sudo \
        wget

# Cleanup
RUN apt-get autoremove -y --purge \
    && apt-get clean 

## # Setup ssh access
## RUN mkdir /var/run/sshd
## RUN echo 'root:password' | chpasswd
## RUN echo X11Forwarding yes >> /etc/ssh/sshd_config
## RUN echo X11UseLocalhost no  >> /etc/ssh/sshd_config
## RUN echo AllowAgentForwarding yes >> /etc/ssh/sshd_config
## RUN echo PermitRootLogin yes >> /etc/ssh/sshd_config
## 
## # SSH login fix. Otherwise user is kicked off after login
## RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
## 
## RUN groupadd --gid $GROUP_ID -r sysop && useradd -m -s /bin/bash --uid $USER_ID -r -g sysop sysop \
##     && echo 'sysop:sysop' | chpasswd 
## 
## RUN mkdir -p /home/sysop/.seiscomp \
##     && chown -R sysop:sysop /home/sysop
## 
## USER root
## ## Start sshd
## RUN passwd -d sysop
## RUN sed -i'' -e's/^#PermitRootLogin prohibit-password$/PermitRootLogin yes/' /etc/ssh/sshd_config \
##     && sed -i'' -e's/^#PasswordAuthentication yes$/PasswordAuthentication yes/' /etc/ssh/sshd_config \
##     && sed -i'' -e's/^#PermitEmptyPasswords no$/PermitEmptyPasswords yes/' /etc/ssh/sshd_config \
##     && sed -i'' -e's/^UsePAM yes/UsePAM no/' /etc/ssh/sshd_config
## 
## USER sysop
## RUN wget https://data.gempa.de/gsm/gempa-gsm.tar.gz &&\
##     tar xvfz gempa-gsm.tar.gz 
## RUN rm -rf gsm/sync
## RUN python3 -m pip install --user configparser cryptography humanize natsort python-dateutil pytz requests tqdm tzlocal
## COPY gsmsetup gsm/
## RUN cd gsm && \
##     bash ./gsmsetup 
## 
## USER root
RUN sed -i 's;apt;apt -y;' $INSTALL_DIR/share/deps/*/*/install-*.sh
RUN $INSTALL_DIR/bin/seiscomp install-deps base gui mariadb-server mariadb-server
COPY seiscomp/ $WORK_DIR/seiscomp-sceewv/
COPY seiscomp/ $INSTALL_DIR/
RUN chown -R sysop:sysop /home/sysop
RUN chown -R sysop:sysop $WORK_DIR/
RUN chown -R sysop:sysop $INSTALL_DIR/
USER sysop

#RUN python3 -m pip install --user pipreqs 
#RUN $INSTALL_DIR/bin/seiscomp print env >> /home/sysop/.bashrc
#RUN sed -i 's;==.*;;' $WORK_DIR/seiscomp-sceewv/requirements.txt
#RUN python3 -m pip install --user -r $WORK_DIR/seiscomp-sceewv/requirements.txt
#RUN mv $WORK_DIR/seiscomp-sceewv/requirements.txt /home/sysop/

ENV PATH="/home/sysop/miniconda/bin:${PATH}"
ARG PATH="/home/sysop/miniconda/bin:${PATH}"
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b -p /home/sysop/miniconda \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 
RUN conda install -c conda-forge pipreqs

RUN python3 -m pipreqs.pipreqs $WORK_DIR/seiscomp-sceewv/bin/
RUN sed -i 's;==.*;;' $WORK_DIR/seiscomp-sceewv/bin/requirements.txt
RUN python3 -m pip install --user -r $WORK_DIR/seiscomp-sceewv/bin/requirements.txt

RUN python3 -m pipreqs.pipreqs $WORK_DIR/seiscomp-sceewv/share/sceewv
RUN sed -i 's;==.*;;' $WORK_DIR/seiscomp-sceewv/share/sceewv/requirements.txt
RUN conda create --name sceewv --channel default --channel anaconda --channel conda-forge gdal
RUN /home/sysop/miniconda/envs/sceewv/bin/pip install -r $WORK_DIR/seiscomp-sceewv/share/sceewv/requirements.txt

RUN rm -rf $WORK_DIR/seiscomp-sceewv 


COPY conf/global.cfg /home/sysop/.seiscomp/
COPY conf/sceewv.cfg /home/sysop/.seiscomp/
RUN ls /home/sysop/.seiscomp/*cfg &&\
    $INSTALL_DIR/bin/seiscomp exec /home/sysop/miniconda/envs/sceewv/bin/python $INSTALL_DIR/bin/sceewv  --query --start "$(date -d "0 day ago" "+%F %T")" --end "$(date -d "now" "+%F %T")" --debug  || true

COPY conf/.mapbox_token $INSTALL_DIR/share/sceewv/
#RUN LD_LIBRARY_PATH=$INSTALL_DIR/seiscomp/lib python3 $INSTALL_DIR/share/sceewv/index.py

USER root
EXPOSE 22
CMD ["sh", "-c", "/usr/sbin/sshd -D"]

