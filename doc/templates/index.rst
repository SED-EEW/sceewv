.. _indexsceewv:

====================
sceewv documentation
====================

This is the documentation for the SCEEWV package a EEW SeisComP review module.

.. _fig-EEW:

.. figure:: media/logo.png
   :width: 60%
   :align: center

About 
-----

This package was developed as part of the ATTAC project, a 
collaboration between SED-ETHZ and some Central American Seismological 
Networks, OVSICORI, MARN, INETER, and INSIVUMEH to implement and test 
an Early Warning System for Central America.

The main objective of this package is to do quality control on the 
performance of the early warning system in addition to computing and 
reviewing alerts on seismological stations performance.


Table of Contents
-----------------

.. toctree::
   :maxdepth: 3

   sceewv
   scqcalert
   sceewv git <https://gitlab.seismo.ethz.ch/SED-EEW/sceewv.git>
   scqcalert git <https://gitlab.seismo.ethz.ch/SED-EEW/scqcalert.git>
